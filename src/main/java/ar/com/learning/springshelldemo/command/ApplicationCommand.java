package ar.com.learning.springshelldemo.command;

import java.util.Random;

import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.client.RestTemplate;

@ShellComponent
public class ApplicationCommand {

	@Autowired
	RestTemplate restTemplate;
	
	// Si le paso a la annotation el atributo "key", le cambio la firma al metodo en la shell, sino toma por default el 
	// 	nombre del metodo.
	@ShellMethod(value = "Adding 2 numbers", key = "sum")
	public int add(int a, int b) {
		return a + b;
	}
	
	@ShellMethod(value = "default value test", key = "sayHello")
	public String hi(@ShellOption(defaultValue = "Niquito") String name) {
		return "Hello " + name;
	}
	
	@ShellMethod(value = "validation", key = "change")
	public String changePassword(@Size(min = 4, max = 10) String password) {
		return "password changed succesfully...";
	}
	
	@ShellMethod(value = "Check connection", key = "connect")
	@ShellMethodAvailability(value = "server connection establishing...")
	public String checkConnection() {
		return isAvailable() ? Availability.available().getReason() 
				: Availability.unavailable("server is not up and running").getReason();
	}
	
	@ShellMethod(value = "Call RestApi", key = "call")
	public String callRestAPI(String url) {
		// Use este para el ejemplo: "https://jsonplaceholder.typicode.com/posts"
		return restTemplate.getForObject(url, String.class);
	}
	
	
	private boolean isAvailable() {
		return new Random().nextBoolean();
	}
}

