# Spring Shell
La idea de este proyecto es tener un approach inicial de Spring Shell.

### Notas
* STS no incluye la dependencia, tuve que ir a buscarla a mano a mvn central.
* Comando "help" printea todos los comandos generados mas los incorporados en la shell.
* El comando script, levanta un archivo y ejecuta todos los comandos de la shell que se encuentran ahi, deje un ejemplo en la carpeta resources. (Lo tuve que ejecutar de la siguiente manera: script C:/Users/Nicolas/eclipse-workspace/SpringShellDemo/src/main/resources/myCommands)


# [Notas Spring docs](https://docs.spring.io/spring-shell/docs/current/reference/htmlsingle/)

What is Spring Shell?
Not all applications need a fancy web user interface! Sometimes, interacting with an application using an interactive terminal is the most appropriate way to get things done.

Spring Shell allows one to easily create such a runnable application, where the user will enter textual commands that will get executed until the program terminates. The Spring Shell project provides the infrastructure to create such a REPL (Read, Eval, Print Loop), allowing the developer to concentrate on the commands implementation, using the familiar Spring programming model.